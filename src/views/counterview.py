from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QGridLayout, QLabel, QPushButton, QWidget

class CounterView(QWidget):

    def __init__(self, parent: Optional[QWidget] = None, flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

        # Init counter value
        self.__count = 0

        # Init ui
        self.__init_ui()

    def __init_ui(self):
        # Init counter label
        self.__counter_label = QLabel("0")

        # Init buttons
        self.__increment_button = QPushButton('+')
        self.__increment_button.pressed.connect(self.__on_increment_button_pressed)

        self.__decrement_button = QPushButton('-')
        self.__decrement_button.pressed.connect(self.__on_decrement_button_pressed)

        self.__reset_button = QPushButton('reset')
        self.__reset_button.pressed.connect(self.__on_reset_button_pressed)
        
        # Init Layout
        self.__main_layout = QGridLayout(parent = self)

        # Add widgets to main layout
        self.__main_layout.addWidget(self.__counter_label, 0, 0, 2, 1, Qt.AlignmentFlag.AlignCenter)
        self.__main_layout.addWidget(self.__increment_button, 0, 1)
        self.__main_layout.addWidget(self.__decrement_button, 0, 2)
        self.__main_layout.addWidget(self.__reset_button, 1, 1, 1, 2)

    def __on_decrement_button_pressed(self):
        # Decrement value
        self.setCount(self.__count - 1)

    def __on_increment_button_pressed(self):
        # Increment value
        self.setCount(self.__count + 1)

    def __on_reset_button_pressed(self):
        # Reset counter
        self.setCount(0)

    def setCount(self, newCount: int):
        if self.__count != newCount:
            # Set new count value
            self.__count = newCount

            # Update label
            self.__counter_label.setText(str(self.__count))
