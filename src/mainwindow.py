from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QMainWindow, QWidget

from src.views.counterview import CounterView

class MainWindow(QMainWindow):

    def __init__(self, parent: Optional[QWidget] = None, \
            flags: Qt.WindowType = Qt.WindowType.Window) -> None:
        super().__init__(parent, flags)

        # Init UI
        self.__init_ui()

    def __init_ui(self):

        # Init counter view
        self.__counter_view = CounterView()

        # Set clock widget as central widget
        self.setCentralWidget(self.__counter_view)
