

if __name__ == "__main__":
    import sys
    from PyQt6.QtWidgets import QApplication

    from src.mainwindow import MainWindow

    # Init app
    app = QApplication(sys.argv)

    # Create a Qt widget, which will be our window.
    window = MainWindow()

    # Display the main window
    window.show()

    # Start the event loop.
    app.exec()
