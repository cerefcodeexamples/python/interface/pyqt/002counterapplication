# 002 - Counter

## Requirements

## Summary
* [1. Init application code](#1-init-application-code)
* [2. Building the counter view](#2-building-the-counter-view)
    * [2.1. Init the view class](#21-init-the-view-class)
    * [2.2. Building the view](#22-building-the-view)
    * [2.3. Initialize and set the count](#23-initialize-and-set-the-count)
    * [2.4. Adding button pressed event](#24-adding-button-pressed-event)
* [3. To go further](#3-to-go-further)
    * [3.1. Add customizable initial count value](#31-add-customizable-initial-count-value)
    * [3.2. Limit range for the counter](#32-limit-range-for-the-counter)

## 1. Init application code
To init the application, the code is exactly the same as for the example 
[001 - First application](https://gitlab.com/cerefcodeexamples/python/interface/pyqt/001firstapplication).
We will take the same `main.py` file and `src/mainwindow.py` as for this 
example.

## 2. Building the counter view
In the first example, we have built a widget that displays the current date &
hour. This simply a label widget which is automatically update each second with
a next text representing the formatted date + hour.

In this example, we don't build a widget but we build a view with multiple
widgets interacting with each other. We will have:
- A label tha displays the current count value
- A button that increments the count value by 1
- A button that decrements the count value by 1
- A button that reset the count value to 0

### 2.1. Init the view class
First step is building a `CounterView` class that inherit from `QWidget` from 
the module `PyQt6.QtWidgets` with a method `__init_ui` where we will put all the
code describing the structure of the view:

```python
from typing import Optional

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QWidget

class CounterView(QWidget):

    def __init__(self, parent: Optional[QWidget] = None, flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

        # Init ui
        self.__init_ui()

    def __init_ui(self):
        ...
```

### 2.2. Building the view
To build the view, we will use a layout that will handle the position of all
widgets for us. For this example, we will use the `QGridLayout` which is grid
layout.

First of all, we will init all widgets without setting their parent:
```python
from PyQt6.QtWidgets import QLabel, QPushButton

...

    def __init__ui(self):

        # Init counter label
        self.__counter_label = QLabel("0")

        # Init control buttons
        self.__increment_button = QPushButton('+')

        self.__decrement_button = QPushButton('-')
        
        self.__reset_button = QPushButton('reset')
```

Then we will init the grid layout with setting the current object (`self`) as
parent:
```python
from PyQt6.QtWidgets import QGridLayout

...

    def __init_ui(self):
        ...

        # Init main layout
        self.__main_layout = QGridLayout(parent=self)
```

Now we can add widgets to the grid layout as shown in the following figure:
![grid_layout_placement](figures/grid_layout_placement.png)

- Counter label placed at (0, 0) and takes two rows height
- Plus button at (0, 1)
- Minus button at (0, 2)
- Reset button at (1, 1) and takes two columns width

To add widgets to the layout you must use the `addWidget(...)` method. There are
three ways to add widgets:
- `addWidget(widget)`, this will place automatically the widget in the grid;
- `addWidget(widget, row, column)`, this will place the widget at the specifies
row and column;
- `addWidget(widget, row, column, row_span, column_span)`, this will place the
widget at the specifies row and column and its size will be `row_span` rows
height and `column_span` columns width.

```python
    def __init_ui(self):
        ...

        # Init main layout
        self.__main_layout.addWidget(self.__counter_label, 0, 0, 2, 1)
        self.__main_layout.addWidget(self.__increment_button, 0, 1)
        self.__main_layout.addWidget(self.__decrement_button, 0, 2)
        self.__main_layout.addWidget(self.__reset_button, 1, 1, 1, 2)
```

### 2.3. Initialize and set the count
The view must keep the count value to ease its update when a button is pressed.
First of all, we will initialize the count value at 0. This must be done ine the
constructor before calling the `__init_ui` method:
```python
    def __init__(self, parent: Optional[QWidget] = None, flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

        # Init counter value
        self.__count = 0

        # Init ui
        self.__init_ui()
```

### 2.4. Adding button pressed event
Now we have built the view with all widgets, we will add all interactions.

The Plus button must increase the count value. So we will make a slot 
`__on_increment_button_pressed` that will increase the count value:
```python
    def __on_increment_button_pressed(self):
        # Increment count value
        self.__count += 1

        # Update label
        self.__counter_label.setText(str(self.__count))
```

The Minus button must decrease the count value. So we will make a slot 
`__on_decrement_button_pressed` that will decrease the count value:
```python
    def __on_increment_button_pressed(self):
        # Decrement count value
        self.__count -= 1

        # Update label
        self.__counter_label.setText(str(self.__count))
```

The Plus button must increase the count value. So we will make a slot 
`__on_increment_button_pressed` that will increase the count value:
```python
    def __on_increment_button_pressed(self):
        # Reset count value
        self.__count = 0

        # Update label
        self.__counter_label.setText(str(self.__count))
```

In the code above, we can point two problems:
1. There is duplicated code for updating the count label
`self.__counter_label.setText(str(self.__count))`;
2. Each time the reset button the label will be updated even if the count value
has not been updated.

So if we resume the two problems, we want to a make code each time the count
value changes value. So we will make a `setCount` method that will check if the
count value changes and update the count value and the label when the count
value changes:
```python
    def setCount(self, newCount: int):
        if self.__count != newCount:
            # Set new count value
            self.__count = newCount

            # Update label
            self.__counter_label.setText(str(self.__count))
```

So we must update the slot methods:
```python
    def __on_decrement_button_pressed(self):
        # Decrement value
        self.setCount(self.__count - 1)

    def __on_increment_button_pressed(self):
        # Increment value
        self.setCount(self.__count + 1)

    def __on_reset_button_pressed(self):
        # Reset counter
        self.setCount(0)
```

The last step is connecting all slots with the right signal. So in the 
`__init__ui` method, we will add the connection between the `pressed` signal of
each button with the right slot just after initializing the button:
```python
    def __init_ui(self):
        ...

        # Init buttons
        self.__increment_button = QPushButton('+')
        self.__increment_button.pressed.connect(self.__on_increment_button_pressed)

        self.__decrement_button = QPushButton('-')
        self.__decrement_button.pressed.connect(self.__on_decrement_button_pressed)

        self.__reset_button = QPushButton('reset')
        self.__reset_button.pressed.connect(self.__on_reset_button_pressed)
```

## 3. To go further

### 3.1. Add customizable initial count value
You can add a parameter to the constructor that will allow us to change the 
initial count value when building a `CounterView`:
```python
    def __init__(self, init_count: int = 0, parent: Optional[QWidget] = None, flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

        # Init counter value
        self.__count = init_count

        # Init ui
        self.__init_ui()
```

In this case, you must set the correct value in the label. To handle, just pass
the `self.__count` value converted into a string when creating the label widget:
```python
    def __init_ui(self):
        # Init counter label
        self.__counter_label = QLabel(str(self.__count))
```

### 3.2. Limit range for the counter
You can add a limit range to the counter view:
```python
    def __init__(self, count: int = 0, min_value: Optional[int] = None, max_value: Optional[int] = None, parent: Optional[QWidget] = None, flags: Qt.WindowType = Qt.WindowType.Widget) -> None:
        super().__init__(parent, flags)

        # Init counter value
        self.__count = count

        # Init limit range value
        self.__min_value = min_value
        self.__max_value = max_value

        # Init ui
        self.__init_ui()
```
So in this case we must adapt the `__on_decrement_button_pressed` and
`__on_increment_button_pressed` to take into account the limits:
```python
    def __on_increment_button_pressed(self):
        # Increment value
        if self.__max_value is None or self.__count < self.__max_value:
            self.setCount(self.__count + 1)
```

```python
    def __on_decrement_button_pressed(self):
        # Decrement value
        if self.__min_value is None or self.__count > self.__min_value:
            self.setCount(self.__count - 1)
```

To go further, in the case `min_value` and `max_value` have been set, you can 
add value check between `min_value` & `max_value` if their own values doesn't 
respect the condition `min_value < max_value`.You can handle this case in 
two ways:
1. Raise a `ValueError` so the program won't continue to run instead if you
handle this raise when creating a `CounterView` object;
2. Invert `min_value` & `max_value` but in this case, you must log a message
that informs the programer or the user that there was an unexpected behavior in
the code.